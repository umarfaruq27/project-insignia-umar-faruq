module.exports = {
  root: true,
  env: {
    node: true,
  },
  extends: [
    'plugin:vue/essential',
    '@vue/airbnb',
  ],
  parserOptions: {
    ecmaVersion: 2020,
  },
  rules: {
    'max-len': 0,
    'spaced-comment': 0,
    'linebreak-style': 0,
    'no-trailing-spaces': 0,
    'no-unused-vars' : 0,
    'semi': 0,
    'comma-dangle': 0,
    'import/extensions': 0,
    'no-unused-components': 0,
    'object-curly-spacing': 0,
    'object-curly-newline': 0,
    'no-console': process.env.NODE_ENV === 'production' ? 'warn' : 'off',
    'no-debugger': process.env.NODE_ENV === 'production' ? 'warn' : 'off',
  },
};
